library(sdmt)

set.seed(12454)

dbcon <- sdmt.createConnection("PostgreSQL test")

train <- sdmt.dbQuery(dbcon,"SELECT * FROM iris")
test <- train[sample(nrow(iris), 25),]


result <- sdmt.classifier(species~.,train,test,clstype = sdmt.NB)
table(result$PredictionOnTestClasses, test$species)

result <- sdmt.classifier(species~.,train,test,clstype = sdmt.KNN, nn = 3)
table(result$PredictionOnTestClasses, test$species)

result <- sdmt.classifier(species~.,train,test,clstype = sdmt.KNN, nn = 1)
table(result$PredictionOnTestClasses, test$species)

result <- sdmt.classifier(species~.,train,test,clstype = sdmt.KNN, nn = 6)
table(result$PredictionOnTestClasses, test$species)

result <- sdmt.classifier(species~.,train,test,clstype = sdmt.DTGBM)
table(result$PredictionOnTestClasses, test$species)

result <- sdmt.classifier(species~.,train,test,clstype = sdmt.DTCLASS)
table(result$PredictionOnTestClasses, test$species)
plot(result$DecisionTree, main="Decision Tree - Classification")
text(result$DecisionTree)

result <- sdmt.classifier(species~.,train,test,clstype = sdmt.DTREG)
table(result$PredictionOnTestClasses, test$species)
plot(result$DecisionTree, main="Decision Tree - Regression")

sdmt.increasedPrediction(species~.,train,test,clstype = sdmt.DTC45,count = 5)

sdmt.increasedPrediction(species~.,train,test,clstype = sdmt.DTC50,count = 5)

sdmt.algrtmComparison(species~.,train[1:150,],test,
                      clstypelist = c(sdmt.NB,sdmt.KNN,sdmt.DTRF,sdmt.DTBAG,
                                            sdmt.DTC45,sdmt.DTC50,sdmt.DTGBM,
                                                    sdmt.DTPART,sdmt.DTCLASS,sdmt.DTREG),nn=3)


result <- sdmt.classifier(species~.,train,test,clstype = sdmt.DTCLASS)
sdmt.dbStorePredictions(Connection = dbcon,TestData = test,Prediction = result$PredictionOnTestClasses,TableName = "iris_prediction")

close(dbcon)