sdmt.installPackages <- function(){
  install.packages("rJava")
  install.packages("RWekajars")
  install.packages("RODBC")
  install.packages("rpart")
  install.packages("RWeka")
  install.packages("ipred")
  install.packages("randomForest")
  install.packages("gbm")
  install.packages("C50")
  install.packages("MASS")
  install.packages("e1071")
  install.packages("party")
  install.packages("class")

}
